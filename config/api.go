package config

import (
	"os"
)

type configuration struct {
	Server serverConfig
}

type serverConfig struct {
	Port string
}

func New() *configuration {
	serv := serverConfig{
		Port: os.Getenv("PORT"),
	}
	conf := configuration{Server: serv}
	return &conf
}
