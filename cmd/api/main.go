package main

import (
	"api/config"
	"api/handler"
	"fmt"
	"log"
	"net/http"
)

func main() {

	conf := config.New()
	serverConfig := conf.Server
	mux := http.NewServeMux()
	mux.HandleFunc("/", handler.HandleIndex)

	s := &http.Server{
		Addr:    ":" + serverConfig.Port,
		Handler: mux,
	}
	fmt.Println("starting")
	if err := s.ListenAndServe(); err != nil && err != http.ErrServerClosed {
		log.Fatal("Server startup failed")
	}
}
