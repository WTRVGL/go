package handler

import (
	"encoding/json"
	"net/http"
)

type cat struct {
	Name string `json:"name"`
	Age  int    `json:"age"`
}

func HandleIndex(w http.ResponseWriter, _ *http.Request) {

	pop := cat{
		Name: "Pop",
		Age:  15,
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(200)
	_ = json.NewEncoder(w).Encode(pop)
}
