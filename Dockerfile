FROM golang:1.13-alpine as build
WORKDIR /app
RUN apk update && apk add --no-cache gcc musl-dev git bash
COPY . .
RUN go build -ldflags '-w -s' -a -o ./bin/app ./cmd/api

FROM alpine
ENV PORT=8080
RUN apk update && apk add --no-cache bash
##COPY --from=build /app/.env .
COPY --from=build /app/bin/app .
CMD ["/app"]

